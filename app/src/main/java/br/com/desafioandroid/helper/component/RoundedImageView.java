package br.com.desafioandroid.helper.component;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 25/07/2016.
 */
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

public class RoundedImageView extends View {

    private Paint mPaint;
    private Rect mRect;
    private Path mPath;

    public RoundedImageView(Context context) {
        this(context, null, 0);
    }

    public RoundedImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoundedImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setColor(Color.BLACK);
        mRect = new Rect();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        // Define um path "ovalado" do tamanho da View.
        RectF r = new RectF();
        r.set(0, 0, (float) w, (float) h);
        mPath = new Path();
        mPath.addOval(r, Path.Direction.CW);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        // Corta o canvas com o path "ovalado".
        canvas.clipPath(mPath);
        // Pinta o fundo de vermelho.
        canvas.drawColor(Color.RED);

        // Desenha o retângulo vermelho da esquerda.
        mRect.set(0, 0, (int) (getWidth() * .25f), getHeight());
        canvas.drawRect(mRect, mPaint);

        // Desenha o retângulo vermelho da direita.
        mRect.set((int) (getWidth() * .75f), 0, getWidth(), getHeight());
        canvas.drawRect(mRect, mPaint);

    }

};