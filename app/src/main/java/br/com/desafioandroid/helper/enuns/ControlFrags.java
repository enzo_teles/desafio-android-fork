package br.com.desafioandroid.helper.enuns;

import br.com.desafioandroid.view.fragment.AbstractFragment;
import br.com.desafioandroid.view.fragment.ContentFragment;
import br.com.desafioandroid.view.fragment.ListPullResquestFragment;
import br.com.desafioandroid.view.fragment.SplashFragment;

/**
 * Here are all enums that will be used in the application, along with its keys and values
 * */
public enum ControlFrags {

	SPLASH	     			("splash", 	 SplashFragment.class),
	ListPull	   			("listpull", ListPullResquestFragment.class),
	CONTENT	     			("content",  ContentFragment.class);

	
	private String name;
	private Class<? extends AbstractFragment> classFrag;
	
	private ControlFrags(final String name, Class<? extends AbstractFragment> classFrag) {
		this.name = name;
		this.classFrag = classFrag;
	}



	public String getName() {
		return name;
	}
	public Class<? extends AbstractFragment> getClassFrag() {
		return classFrag;
	}
}
