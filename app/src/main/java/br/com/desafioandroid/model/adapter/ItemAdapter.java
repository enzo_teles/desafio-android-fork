package br.com.desafioandroid.model.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;

import br.com.desafioandroid.R;
import br.com.desafioandroid.helper.component.RoundedImageView;
import br.com.desafioandroid.helper.proportion.DisplayUtil;
import br.com.desafioandroid.model.pojo.Item;
import butterknife.Bind;
import butterknife.ButterKnife;
import jp.wasabeef.picasso.transformations.CropCircleTransformation;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 25/07/2016.
 */
public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.Holder> {

    private List<Item>      itemList;
    private Activity        activity;

    public ItemAdapter(List<Item> itemList, Activity activity) {
        this.itemList       = itemList;
        this.activity       = activity;
    }

    @Override
    public ItemAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view           = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter, parent, false);
        DisplayUtil         .setLayoutParams((ViewGroup) view.findViewById(R.id.layout_adapter));
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(ItemAdapter.Holder holder, int position) {

        Item item = itemList.get(position);
        String path = item.getOwner().getAvatarUrl()+"";
        holder.nameRep.setText(item.getFullName()+"");
        holder.description.setText(item.getDescription()+"");
        holder.username.setText(item.getOwner().getLogin()+"");
        holder.nameUser.setText(item.getName()+"");
        holder.commits.setText(item.getForksCount()+"");
        holder.favorites.setText(item.getStargazersCount()+"");
        Picasso.with(activity)
                .load(path)
                .transform(new CropCircleTransformation())
                .into(holder.photo);

    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @Bind(R.id.name_rep)
        TextView               nameRep;
        @Bind(R.id.desc_repo)
        TextView               description;
        @Bind(R.id.username)
        TextView               username;
        @Bind(R.id.name_user)
        TextView               nameUser;
        @Bind(R.id.commits)
        TextView               commits;
        @Bind(R.id.favorites)
        TextView               favorites;
        @Bind(R.id.photo)
        ImageView        photo;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
