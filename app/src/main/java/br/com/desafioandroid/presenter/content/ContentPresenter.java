package br.com.desafioandroid.presenter.content;

import android.support.v4.widget.SwipeRefreshLayout;

import java.util.List;

import br.com.desafioandroid.model.pojo.Item;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 25/07/2016.
 */
public class ContentPresenter implements OnContentPresenter, OnContentCallback {

    private OnContentView           onContentView;
    private OnContentInteractor     onContentInteractor;

    public ContentPresenter(OnContentView onContentView) {
        this.onContentView          = onContentView;
        onContentInteractor         = new ContentInteractor();
    }

    @Override
    public void callContent(SwipeRefreshLayout mSwipeRefreshLayout, int page) {
        onContentInteractor         .callContent(this, mSwipeRefreshLayout, page);
    }

    @Override
    public void resultList(List<Item> itemList){
        onContentView.resultList(itemList);
    }
}
