package br.com.desafioandroid.presenter.content;

import android.support.v4.widget.SwipeRefreshLayout;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 25/07/2016.
 */
public interface OnContentPresenter {
    void callContent(SwipeRefreshLayout mSwipeRefreshLayout, int page);
}
