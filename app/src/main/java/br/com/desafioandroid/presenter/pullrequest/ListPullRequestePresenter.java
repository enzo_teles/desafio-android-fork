package br.com.desafioandroid.presenter.pullrequest;

import android.support.v4.widget.SwipeRefreshLayout;

import java.util.List;

import br.com.desafioandroid.helper.log.WrapperLog;
import br.com.desafioandroid.model.pojo.PullResquest;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 26/07/2016.
 */
public class ListPullRequestePresenter implements OnListPullRequestPresenter, OnListPullRequestCallback {

    private OnListPullRequestView               onListPullRequestView;
    private OnListPullRequestInteractor         onListPullRequestInteractor;

    /**
     * method to show of the list pullrequest
     * */
    public ListPullRequestePresenter(OnListPullRequestView onListPullRequestView) {
        this.onListPullRequestView              = onListPullRequestView;
        this.onListPullRequestInteractor        = new ListPullRequestInterator();
    }

    /**
     * method to call the interactor
     * */
    @Override
    public void callPullRequest(SwipeRefreshLayout mSwipeRefreshLayout, String user, String repository) {
        onListPullRequestInteractor.callPullRequeste(this, user, repository, mSwipeRefreshLayout);

    }

    /**
     * method to return list pullrequest
     * */
    @Override
    public void returnListPullRequest(List<PullResquest> listPullRequest) {
        onListPullRequestView.returnListPullRequest(listPullRequest);
    }
}
