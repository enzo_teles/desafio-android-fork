package br.com.desafioandroid.presenter.pullrequest;

import java.util.List;

import br.com.desafioandroid.model.pojo.PullResquest;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 26/07/2016.
 */
public interface OnListPullRequestCallback {
    void returnListPullRequest(List<PullResquest> listPullRequest);
}
