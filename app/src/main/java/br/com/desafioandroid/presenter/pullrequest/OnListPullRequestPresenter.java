package br.com.desafioandroid.presenter.pullrequest;

import android.support.v4.widget.SwipeRefreshLayout;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 26/07/2016.
 */
public interface OnListPullRequestPresenter {
    void callPullRequest(SwipeRefreshLayout mSwipeRefreshLayout, String user, String repository);
}
