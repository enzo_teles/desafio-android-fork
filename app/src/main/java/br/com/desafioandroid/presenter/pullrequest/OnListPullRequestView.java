package br.com.desafioandroid.presenter.pullrequest;

import android.support.v4.widget.SwipeRefreshLayout;

import java.util.List;

import br.com.desafioandroid.model.pojo.PullResquest;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 26/07/2016.
 */
public interface OnListPullRequestView {
    void callListPull(SwipeRefreshLayout mSwipeRefreshLayout, String user, String repository);
    void returnListPullRequest(List<PullResquest> listPullRequest);
}
