package br.com.desafioandroid.presenter.splash;
/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 26/07/2016.
 */
public interface OnSplashCallback {

	public boolean showMsg(String msg);
	

}
