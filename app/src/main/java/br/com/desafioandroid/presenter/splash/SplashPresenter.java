package br.com.desafioandroid.presenter.splash;
/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 26/07/2016.
 */
public class SplashPresenter implements OnSplashPresenter, OnSplashCallback {


	private transient OnSplashView onSplashView;
	private transient OnSplashInteractor onSplashInteractor;
	

	public SplashPresenter(OnSplashView onSplashView) {
		// TODO Auto-generated constructor stub
		this.onSplashView = onSplashView;
		this.onSplashInteractor = new SplashInteractor();

	}

	/**
	 * method to delegate activity to the interactor
	 * */
	@Override
	public boolean callSplash() {
		// TODO Auto-generated method stub
		onSplashInteractor.callSplash(this);
		return true;
		
	}
	/**
	 * method to show the return message of the callback
	 * */
	@Override
	public boolean showMsg(String msg) {
		// TODO Auto-generated method stub
		if(msg != null){
			onSplashView.showMessage("msg "+ msg);
			return true;
		}
		return false;
	}

}
