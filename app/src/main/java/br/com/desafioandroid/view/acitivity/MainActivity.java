package br.com.desafioandroid.view.acitivity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import br.com.desafioandroid.R;
import br.com.desafioandroid.helper.enuns.ControlFrags;
import br.com.desafioandroid.view.fragment.SplashFragment;
import br.com.desafioandroid.view.listener.OnMainActivityView;
import butterknife.Bind;
import butterknife.ButterKnife;
/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 26/07/2016.
 */
public class MainActivity extends AbstractActivity implements OnMainActivityView {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        if (savedInstanceState == null) {
            addFragment(ControlFrags.SPLASH, R.id.content, false);

        }else{
            addFragment(ControlFrags.SPLASH, R.id.content, false);
        }
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        /*getSupportActionBar().setDisplayShowHomeEnabled(true);
        NavigationDrawerFragment drawerFragment;
        drawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.navigation_drawer);
        drawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout)findViewById(R.id.layout_navigation), toolbar);*/
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * method to call the fragment
     */
    @Override
    public void registrerFragment(ControlFrags frags, int layoutId, boolean addBackStack) {
        registrerFragment(frags, layoutId, addBackStack);
    }

    /**
     * method to transfer the fragment
     */

    @Override
    public void transferFragment(ControlFrags frags, int layoutId, boolean addBackStack) {
        replaceFragment(frags, layoutId, addBackStack);
    }

    /**
     * method to send bundle to another fragment
     */

    @Override
    public void transferFragment(ControlFrags frags, int layoutId, boolean addBackStack, Bundle bundle) {
        replaceFragment(frags, layoutId, addBackStack, bundle);
    }

    @Override
    protected void onSaveInstanceState (Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("splash", ControlFrags.SPLASH);
    }

}
