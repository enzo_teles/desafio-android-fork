package br.com.desafioandroid.view.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import br.com.desafioandroid.R;
import br.com.desafioandroid.helper.component.EndlessRecyclerOnScrollListener;
import br.com.desafioandroid.helper.enuns.ControlFrags;
import br.com.desafioandroid.helper.log.WrapperLog;
import br.com.desafioandroid.helper.proportion.DisplayUtil;
import br.com.desafioandroid.model.adapter.ItemAdapter;
import br.com.desafioandroid.model.pojo.Item;
import br.com.desafioandroid.presenter.content.ContentPresenter;
import br.com.desafioandroid.presenter.content.OnContentPresenter;
import br.com.desafioandroid.presenter.content.OnContentView;
import br.com.desafioandroid.view.listener.OnMainActivityView;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 26/07/2016.
 */
@SuppressLint("NewApi")
public class ContentFragment extends AbstractFragment implements OnContentView, SwipeRefreshLayout.OnRefreshListener {

	private OnContentPresenter 				onContentPresenter;
	private OnMainActivityView 				onMainActivityView;
	private ItemAdapter						itemAdapter;
	@Bind(R.id.swipe_container)
	SwipeRefreshLayout 						mSwipeRefreshLayout;
	@Bind(R.id.recyclerView)
	RecyclerView 							recyclerView;
	//progress
	@Bind(R.id.progressBar)
	ProgressBar 							mProgressBar;
	int										page = 1;
	int										count = 0;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view 							= inflater.inflate(R.layout.content, container, false);
		DisplayUtil							.setLayoutParams((ViewGroup) view.findViewById(R.id.layout_content));
		ButterKnife							.bind(this, view);
		onMainActivityView 					= (OnMainActivityView) getActivity();
		onMainActivityView					.getToolbar().setVisibility(View.VISIBLE);
		onMainActivityView					.getToolbar().setTitle(getResources().getString(R.string.title));
		onMainActivityView                  .getToolbar().setNavigationIcon(null);

		//progressBar
		mProgressBar		.setVisibility(View.VISIBLE);

		//swiper
		mSwipeRefreshLayout.setOnRefreshListener(ContentFragment.this);
		mSwipeRefreshLayout.setColorScheme(android.R.color.holo_blue_bright,
				android.R.color.holo_green_light,
				android.R.color.holo_orange_light,
				android.R.color.holo_red_light);

		onContentPresenter					= new ContentPresenter(this);
		callContent(mSwipeRefreshLayout, page);

		return view;
	}

	/**
	 * method to call the presenter
	 * */
	@Override
	public void callContent(SwipeRefreshLayout mSwipeRefreshLayout, int page) {
		onContentPresenter.callContent(mSwipeRefreshLayout, page);
	}

	/**
	 * method to show the list the item
	 * */
	@Override
	public void resultList(final List<Item> itemList){

		WrapperLog.info("itemlist.content " + itemList.size());
		mProgressBar					.setVisibility(View.GONE);
		itemAdapter 					= new ItemAdapter(itemList, getActivity());
		RecyclerView					.LayoutManager manager = new LinearLayoutManager(getActivity());
		recyclerView					.setLayoutManager(manager);
		recyclerView					.setHasFixedSize(true);
		recyclerView					.setItemAnimator(new DefaultItemAnimator());
		recyclerView					.setAdapter(itemAdapter);
		itemAdapter						.notifyDataSetChanged();

		recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
			@Override
			public void onClick(View view, int position) {
				Item item = itemList.get(position);
				Bundle b = new Bundle();
				b.putString("user", item.getOwner().getLogin());
				b.putString("repository", item.getName());

				onMainActivityView.transferFragment(ControlFrags.ListPull, R.id.content, true, b);
			}

			@Override
			public void onLongClick(View view, int position) {

			}
		}));

		recyclerView.setOnScrollListener(new EndlessRecyclerOnScrollListener((LinearLayoutManager) manager) {
			@Override
			public void onLoadMore(int current_page) {
				// do something...
				mProgressBar					.setVisibility(View.VISIBLE);
				count = count + current_page;
				callContent(mSwipeRefreshLayout, count);
			}
		});
	}

	@Override
	public void onRefresh() {
		new Handler().postDelayed(new Runnable() {
			@Override public void run() {
				mSwipeRefreshLayout.setRefreshing(false);
				callContent(mSwipeRefreshLayout, page);
			}
		}, 5000);
	}

//RecyclerView
public interface ClickListener {
	void onClick(View view, int position);

	void onLongClick(View view, int position);
}

public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

	private GestureDetector gestureDetector;
	private ContentFragment.ClickListener clickListener;

	public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ContentFragment.ClickListener clickListener) {
		this.clickListener = clickListener;
		gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
			@Override
			public boolean onSingleTapUp(MotionEvent e) {
				return true;
			}

			@Override
			public void onLongPress(MotionEvent e) {
				View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
				if (child != null && clickListener != null) {
					clickListener.onLongClick(child, recyclerView.getChildPosition(child));
				}
			}
		});
	}

	@Override
	public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

		View child = rv.findChildViewUnder(e.getX(), e.getY());
		if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
			clickListener.onClick(child, rv.getChildPosition(child));
		}
		return false;
	}

	@Override
	public void onTouchEvent(RecyclerView rv, MotionEvent e) {
	}

	@Override
	public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

	}
	}

}
