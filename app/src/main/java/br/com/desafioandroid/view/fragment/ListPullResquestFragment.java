package br.com.desafioandroid.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import br.com.desafioandroid.R;
import br.com.desafioandroid.helper.enuns.ControlFrags;
import br.com.desafioandroid.helper.log.WrapperLog;
import br.com.desafioandroid.helper.proportion.DisplayUtil;
import br.com.desafioandroid.helper.service.RestApi;
import br.com.desafioandroid.model.adapter.PullAdapter;
import br.com.desafioandroid.model.pojo.Item;
import br.com.desafioandroid.model.pojo.PullResquest;
import br.com.desafioandroid.presenter.pullrequest.ListPullRequestePresenter;
import br.com.desafioandroid.presenter.pullrequest.OnListPullRequestPresenter;
import br.com.desafioandroid.presenter.pullrequest.OnListPullRequestView;
import br.com.desafioandroid.view.listener.OnMainActivityView;
import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 26/07/2016.
 */
public class ListPullResquestFragment extends AbstractFragment implements OnListPullRequestView, SwipeRefreshLayout.OnRefreshListener {

    private OnListPullRequestPresenter      onListPullRequestPresenter;
    private OnMainActivityView              onMainActivityView;
    private String                          user;
    private String                          repository;

    private PullAdapter                     pullAdapter;
    @Bind(R.id.swipe_container)
    SwipeRefreshLayout                      mSwipeRefreshLayout;
    @Bind(R.id.recyclerView)
    RecyclerView                            recyclerView;
    //progress
    @Bind(R.id.progressBar)
    ProgressBar                             mProgressBar;
    @Bind(R.id.opened)
    TextView                                opened;
    @Bind(R.id.closed)
    TextView                                closed;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view                           = inflater.inflate(R.layout.pull_request, container, false);
        onMainActivityView                  = (OnMainActivityView) getActivity();
        DisplayUtil                         .setLayoutParams((ViewGroup) view.findViewById(R.id.layout_pull));
        ButterKnife                         .bind(this, view);
        onListPullRequestPresenter          = new ListPullRequestePresenter(this);

        //progressBar
        mProgressBar		.setVisibility(View.VISIBLE);

        //swiper
        mSwipeRefreshLayout.setOnRefreshListener(ListPullResquestFragment.this);
        mSwipeRefreshLayout.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        if(getArguments() != null){
            user                             = getArguments().getString("user");
            repository                       = getArguments().getString("repository");
        }
        onMainActivityView                  .getToolbar().setTitle(user+"/"+repository);
        onMainActivityView                  .getToolbar().setNavigationIcon(R.drawable.arrow_left);
        onMainActivityView.getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if( keyCode == KeyEvent.KEYCODE_BACK ) {
                    getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    return true;
                } else {
                    return false;
                }
            }
        });



        callListPull(mSwipeRefreshLayout, user, repository);
        return view;
    }

    /**
     * method to call the pullrequest presenter
     * */
    @Override
    public void callListPull(SwipeRefreshLayout mSwipeRefreshLayout, String user, String repository) {
        onListPullRequestPresenter.callPullRequest(mSwipeRefreshLayout, user, repository);
    }

    /**
     * method to show pullrequest list1
     * */
    @Override
    public void returnListPullRequest(final List<PullResquest> listPullRequest) {

        int open = 0;
        int close = 0;

        WrapperLog.info("listPullsize " + listPullRequest.size());

        for (PullResquest p : listPullRequest){
            if(p.getHead().getRepo() != null )
                open  +=  p.getHead().getRepo().getOpenIssues();
            if(p.getHead().getRepo()!= null)
            close +=  p.getHead().getRepo().getForksCount();
        }

        opened.setText(open+" opened ");
        closed.setText("/ " +close+" closed");

        mProgressBar					.setVisibility(View.GONE);
        pullAdapter 					= new PullAdapter(this, listPullRequest, getActivity());
        RecyclerView					.LayoutManager manager = new LinearLayoutManager(getActivity());
        recyclerView					.setLayoutManager(manager);
        recyclerView					.setHasFixedSize(true);
        recyclerView					.setItemAnimator(new DefaultItemAnimator());
        recyclerView					.setAdapter(pullAdapter);
        pullAdapter						.notifyDataSetChanged();

        recyclerView.addOnItemTouchListener(new ContentFragment.RecyclerTouchListener(getActivity(), recyclerView, new ContentFragment.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                PullResquest item = listPullRequest.get(position);

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(item.getHtmlUrl()));
                startActivity(i);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    @Override
    public void onRefresh() {

        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                mSwipeRefreshLayout.setRefreshing(false);
                callListPull(mSwipeRefreshLayout, user, repository);
            }
        }, 5000);

    }
    //RecyclerView
    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ContentFragment.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ContentFragment.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

}
