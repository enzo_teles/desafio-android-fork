package br.com.desafioandroid;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import br.com.desafioandroid.helper.enuns.ControlFrags;
import br.com.desafioandroid.view.acitivity.AbstractActivity;
import br.com.desafioandroid.view.acitivity.MainActivity;
import br.com.desafioandroid.view.fragment.ContentFragment;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * @version 1.0
 * @autor Enzo Teles
 * @date 27/07/2016.
 */

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class)
public class ContentFragmentTest {

    private MainActivity activity;
    private ContentFragment  contentFragment;

    @Before
    public void setUp() throws Exception {
        activity = Robolectric.buildActivity(MainActivity.class).create().start().resume().get();
        //contentFragment = new ContentFragment();*/
        contentFragment = (ContentFragment) ContentFragment.instantiate(activity, "content");

    }

    @Test
    public void buttonClickShouldStartNewActivity(){

        String title = activity.getResources().getString(R.string.title);
        assertThat(title, equalTo("GitHub JavaPop"));
    }

}
















